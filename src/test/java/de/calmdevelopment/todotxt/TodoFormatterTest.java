/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TodoFormatterTest {
    @Test
    void format_simple_todo() {
        var todo = Todo.builder().withDescription("A simple Todo").build();

        String todoLine = new TodoFormatter().format(todo);

        assertThat(todoLine).isEqualTo("A simple Todo");
    }

    @Test
    void format_todo_with_priority() {
        var todo = Todo.builder().withDescription("A simple Todo").withPrio(Prio.A).build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(A) A simple Todo");
    }

    @Test
    void format_todo_with_creation_date() {
        var todo = Todo.builder()
                .withDescription("A simple Todo")
                .withPrio(Prio.Q)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(Q) 2022-10-13 A simple Todo");
    }

    @Test
    void format_todo_with_projects() {
        var todo = Todo.builder()
                .withDescription("A simple Todo +tidy-kitchen +project-lambda")
                .withPrio(Prio.A)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withProjects(List.of("tidy-kitchen", "project-lambda"))
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(A) 2022-10-13 A simple Todo +tidy-kitchen +project-lambda");
    }

    @Test
    void format_todo_with_contexts() {
        var todo = Todo.builder()
                .withDescription("A simple Todo @phone @laptop")
                .withPrio(Prio.C)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withContexts(List.of("phone", "laptop"))
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(C) 2022-10-13 A simple Todo @phone @laptop");
    }

    @Test
    void format_todo_with_metadata() {
        var todo = Todo.builder()
                .withDescription("A simple Todo due:2022-12-12 pomodoro:3")
                .withPrio(Prio.A)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withMetadata(new HashMap<>() {{
                    put("due", "2022-12-12");
                    put("pomodoro", "3");
                }})
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(A) 2022-10-13 A simple Todo due:2022-12-12 pomodoro:3");
    }

    @Test
    void format_full_incomplete_todo() {
        var todo = Todo.builder()
                .withDescription("A simple Todo +tidy-kitchen +project-lambda @phone @laptop due:2022-12-12 pomodoro:3")
                .withPrio(Prio.A)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withContexts(List.of("phone", "laptop"))
                .withProjects(List.of("tidy-kitchen", "project-lambda"))
                .withMetadata(new HashMap<>() {{
                    put("due", "2022-12-12");
                    put("pomodoro", "3");
                }})
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("(A) 2022-10-13 A simple Todo +tidy-kitchen +project-lambda @phone @laptop due:2022-12-12 pomodoro:3");
    }

    @Test
    void format_full_complete_todo() {
        var todo = Todo.builder()
                .isCompleted(true)
                .withDescription("A simple Todo +tidy-kitchen +project-lambda @phone @laptop due:2022-12-12 pomodoro:3")
                .withPrio(Prio.A)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withCompletionDate(LocalDate.parse("2022-10-14"))
                .withContexts(List.of("phone", "laptop"))
                .withProjects(List.of("tidy-kitchen", "project-lambda"))
                .withMetadata(new HashMap<>() {{
                    put("due", "2022-12-12");
                    put("pomodoro", "3");
                }})
                .build();

        String todoLine = TodoFormatter.format(todo);

        assertThat(todoLine).isEqualTo("x (A) 2022-10-14 2022-10-13 A simple Todo +tidy-kitchen +project-lambda @phone @laptop due:2022-12-12 pomodoro:3");
    }
}
