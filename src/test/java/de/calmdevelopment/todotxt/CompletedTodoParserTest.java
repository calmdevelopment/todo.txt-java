/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import de.calmdevelopment.todotxt.exception.InvalidTodoFormat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeParseException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CompletedTodoParserTest {
    private TodoParser parser;

    @BeforeEach
    void setUp() {
        parser = new TodoParser();
    }

    @Test
    void parse_starting_with_lowercase_x_followed_by_space() {
        var todo = parser.parse("x Add functionality to parse completed tasks");

        assertThat(todo.completed()).isTrue();
        assertThat(todo.completionDate()).isNull();
        assertThat(todo.creationDate()).isNull();
    }

    @Test
    void a_completed_task_may_have_a_completion_date() {
        var todo = parser.parse("x 2022-10-12 Add functionality to parse completed tasks with creation date");

        assertThat(todo.completed()).isTrue();
        assertThat(todo.completionDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 12));
        assertThat(todo.creationDate()).isNull();
    }

    @Test
    void a_completed_task_may_have_a_completion_date_before_a_creation_date() {
        var todo = parser.parse("x 2022-10-12 2022-10-10 Add functionality to parse completed tasks with creation date");

        assertThat(todo.completed()).isTrue();
        assertThat(todo.creationDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 10));
        assertThat(todo.completionDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 12));
    }

    @Test
    void can_parse_a_completed_full_structured_todo() {
        var todo = parser.parse("x (A) 2022-10-12 2022-10-10 A completed todo with projects +new-kitchen-furniture +refurbish-house contexts @home @kitchen due:2022-10-13 pomodoro:2");

        assertThat(todo.completed()).isTrue();
        assertThat(todo.prio()).isEqualTo(Prio.A);
        assertThat(todo.creationDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 10));
        assertThat(todo.completionDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 12));
        assertThat(todo.description()).isEqualTo("A completed todo with projects +new-kitchen-furniture +refurbish-house contexts @home @kitchen due:2022-10-13 pomodoro:2");
        assertThat(todo.projects()).contains("new-kitchen-furniture", "refurbish-house");
        assertThat(todo.contexts()).contains("home", "kitchen");
        assertThat(todo.metadata()).containsKeys("due", "pomodoro");
        assertThat(todo.metadata()).containsValues("2022-10-13", "2");
    }

    @Test
    void throws_exception_if_date_is_invalid() {
        assertThatExceptionOfType(InvalidTodoFormat.class)
                .isThrownBy(() -> parser.parse("x (A) 2022-14-12 2022-12-13 This is a task with an invalid date"))
                .withMessage("Invalid date (format: yyyy-mm-dd). Text '2022-14-12' could not be parsed: Invalid value for MonthOfYear (valid values 1 - 12): 14")
                .withCauseInstanceOf(DateTimeParseException.class);
    }

}
