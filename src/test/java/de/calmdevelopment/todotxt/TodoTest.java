/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TodoTest {

    @Test
    void projects_are_immutable() {
        var todo = Todo.builder().withDescription("test record behaviour").build();

        Assertions.assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(() -> todo.projects().add("projectX"));

    }

    @Test
    void contexts_are_immutable() {
        var todo = Todo.builder().withDescription("test record behaviour").build();

        Assertions.assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(() -> todo.contexts().add("phone"));
    }

    @Test
    void metadata_are_immutable() {
        var todo = Todo.builder().withDescription("test record behaviour").withMetadata(new HashMap<>() {{
            put("key", "value");
        }}).build();

        Assertions.assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(() -> todo.metadata().put("another", "value"));

        Assertions.assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(() -> todo.metadata().replace("key", "value", "changedValue"));
    }

    @Test
    void access_full_format() {
        var todo = Todo.builder()
                .isCompleted(true)
                .withDescription("A simple Todo with an url https://example.com to +tidy-kitchen and related to +project-lambda @phone @laptop due:2022-12-12 pomodoro:3")
                .withPrio(Prio.A)
                .withCreationDate(LocalDate.parse("2022-10-13"))
                .withCompletionDate(LocalDate.parse("2022-10-14"))
                .withContexts(List.of("phone", "laptop"))
                .withProjects(List.of("tidy-kitchen", "project-lambda"))
                .withMetadata(new HashMap<>() {{
                    put("due", "2022-12-12");
                    put("pomodoro", "3");
                }})
                .build();

        assertThat(todo.full()).isEqualTo("x (A) 2022-10-14 2022-10-13 A simple Todo with an url https://example.com to +tidy-kitchen and related to +project-lambda @phone @laptop due:2022-12-12 pomodoro:3");
    }

    @Test
    void change_prio() {
        var todo = Todo.builder().withDescription("a task that will change priority").build();

        var changed = todo.prioOf(Prio.A);
        assertThat(changed.prio()).isNotEqualTo(todo.prio());
        assertThat(changed.prio()).isEqualTo(Prio.A);
    }

    @Test
    void a_prio_null_value_defaults_to_NONE() {
        var todo = Todo.builder().withDescription("A task").withPrio(null).build();

        assertThat(todo.prio()).isEqualTo(Prio.NONE);
    }

}
