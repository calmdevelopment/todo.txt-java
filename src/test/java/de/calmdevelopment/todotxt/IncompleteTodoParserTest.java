/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import de.calmdevelopment.todotxt.exception.InvalidTodoFormat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class IncompleteTodoParserTest {
    private TodoParser parser;

    @BeforeEach
    void setUp() {
        parser = new TodoParser();
    }

    @Test
    void parse_a_task_with_priority() {
        Todo todo = parser.parse("(A) A task with priority");

        assertThat(todo.prio()).isEqualTo(Prio.A);
        assertThat(todo.description()).isEqualTo("A task with priority");
        assertThat(todo.creationDate()).isNull();
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            A task without priority
            (a) task without priority
            (B)->Submit package
            This i also a task (A) without prio
            xylophone lesson
            X Make resolutions
            """)
    void parse_a_simple_todo(String task) {
        Todo todo = parser.parse(task);

        assertThat(todo.description()).isEqualTo(task);
        assertThat(todo.prio()).isEqualTo(Prio.NONE);
        assertThat(todo.projects()).isEmpty();
        assertThat(todo.contexts()).isEmpty();
        assertThat(todo.metadata()).isEmpty();
        assertThat(todo.creationDate()).isNull();
        assertThat(todo.completionDate()).isNull();
        assertThat(todo.completed()).isFalse();
    }

    @Test
    void throw_an_exception_if_line_is_empty() {
        assertThatExceptionOfType(InvalidTodoFormat.class)
                .isThrownBy(() -> parser.parse(""))
                .withMessage("An empty line is invalid");
    }

    @Test
    void throw_an_exception_if_line_is_null() {
        String invalidTodoLine = null;
        assertThatExceptionOfType(InvalidTodoFormat.class)
                .isThrownBy(() -> parser.parse(invalidTodoLine))
                .withMessage("Null reference is invalid");
    }

    @Test
    void throw_an_exception_on_line_terminators() {
        assertThatExceptionOfType(InvalidTodoFormat.class)
                .isThrownBy(() -> parser.parse("A task with \t\t\tcarriage\nreturn in it\n"))
                .withMessage("Todo does not match the format pattern. given: 'A task with \t\t\tcarriage\nreturn in it\n'");
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            2022-10-06 implement parser to accept creation date
            (A) 2022-10-06 implement parser to accept creation date
            """)
    void parse_todo_with_creation_date(String todoWithCreationDate) {
        var todo = parser.parse(todoWithCreationDate);

        assertThat(todo.creationDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 6));
        assertThat(todo.description()).isEqualTo("implement parser to accept creation date");
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            A todo with a context @code, code
            A todo with a context @nested.context, nested.context
            A todo with a context @01-Abc, 01-Abc
            A todo with a context @🧐, 🧐
            """)
    void parse_todo_with_context(String todoLine, String expectedContext) {
        var todo = parser.parse(todoLine);

        assertThat(todo.contexts()).contains(expectedContext);
    }

    @Test
    void parse_todo_with_multiple_contexts() {
        var todo = parser.parse("A todo with a context @code @java @computer");

        assertThat(todo.contexts()).containsAnyOf("code", "java", "computer");
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            A todo with a context +todo.txt-java, todo.txt-java
            A todo with a context +!urgent-Project!, !urgent-Project!
            A todo with a context +nested.project, nested.project
            A todo with a context +🧐, 🧐
            """)
    void parse_todo_with_project(String todoLine, String expectedProject) {
        var todo = parser.parse(todoLine);

        assertThat(todo.projects()).contains(expectedProject);
    }

    @Test
    void parse_todo_with_multiple_projects() {
        var todo = parser.parse("Plant some seeds +Family +House +Gardening");

        assertThat(todo.projects()).containsAnyOf("Family", "House", "Gardening");
    }

    @Test
    void a_description_does_not_contain_creation_date_and_prioty() {
        var todo = parser.parse("(C) 2022-10-12 Plant some seeds @garden +Family +House +Gardening");

        assertThat(todo.description()).isEqualTo("Plant some seeds @garden +Family +House +Gardening");
    }

    @Test
    void only_first_date_will_be_interpreted_as_created_date() {
        var todo = parser.parse("(C) 2022-10-12 2022-10-11 2022-10-09 Plant some seeds @garden +Family +House +Gardening");

        assertThat(todo.creationDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 12));
        assertThat(todo.completionDate()).isNull();
        assertThat(todo.description()).isEqualTo("2022-10-11 2022-10-09 Plant some seeds @garden +Family +House +Gardening");
    }

    @Test
    void parse_todo_with_metadata_tag() {
        var expectedMetadata = new HashMap<String, String>() {{
            put("due", "2022-10-14");
        }};
        var todo = parser.parse("Take the trash out due:2022-10-14");

        assertThat(todo.metadata()).containsAllEntriesOf(expectedMetadata);
    }

    @Test
    void parse_todo_with_multiple_metadata_tags() {
        var expectedMetadata = new HashMap<String, String>() {{
            put("due", "2022-10-14");
            put("prio", "A");
            put("pomodoro", "2");
        }};
        var todo = parser.parse("Take the trash out pomodoro:2 prio:A due:2022-10-14");

        assertThat(todo.metadata()).containsAllEntriesOf(expectedMetadata);
    }

    @Test
    void can_parse_a_full_structured_todo() {
        var todo = parser.parse("(Z) 2022-10-10 A completed todo with projects +new-kitchen-furniture +refurbish-house contexts @home @kitchen due:2022-10-13 pomodoro:2");

        assertThat(todo.completed()).isFalse();
        assertThat(todo.prio()).isEqualTo(Prio.Z);
        assertThat(todo.creationDate()).isEqualTo(LocalDate.of(2022, Month.OCTOBER, 10));
        assertThat(todo.completionDate()).isNull();
        assertThat(todo.description()).isEqualTo("A completed todo with projects +new-kitchen-furniture +refurbish-house contexts @home @kitchen due:2022-10-13 pomodoro:2");
        assertThat(todo.projects()).contains("new-kitchen-furniture", "refurbish-house");
        assertThat(todo.contexts()).contains("home", "kitchen");
        assertThat(todo.metadata()).containsKeys("due", "pomodoro");
        assertThat(todo.metadata()).containsValues("2022-10-13", "2");
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            A Todo invalid: with invalid tag
            A Todo :invalid with invalid tag
            A Todo \t:invalid with invalid tag
            A Todo :\t with invalid tag
            A Todo with:invalid:metadatatag
            """)
    void parse_todo_with_invalid_metadata(String todoLine) {
        var todo = parser.parse(todoLine);

        assertThat(todo.description()).isEqualTo(todoLine);
        assertThat(todo.metadata()).isEmpty();
    }

    @Test
    void a_url_is_not_a_metatag() {
        var todo = parser.parse("A task to @watch https://www.youtube.com/watch?v=IA-RyzKE7oc&list=WL&index=2&t=2663s");

        assertThat(todo.metadata()).isEmpty();
    }

    @Test
    void throws_exception_if_date_is_invalid() {
        assertThatExceptionOfType(InvalidTodoFormat.class)
                .isThrownBy(() -> parser.parse("(A) 2022-12-32 This is a task with an invalid date"))
                .withMessage("Invalid date (format: yyyy-mm-dd). Text '2022-12-32' could not be parsed: Invalid value for DayOfMonth (valid values 1 - 28/31): 32")
                .withCauseInstanceOf(DateTimeParseException.class);
    }
}
