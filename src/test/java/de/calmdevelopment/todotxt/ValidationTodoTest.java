/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

public class ValidationTodoTest {
    @Test
    void empty_description_is_invalid() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> Todo.builder().withDescription("").build())
                .withMessage("description cannot be empty");
    }

    @Test
    void null_description_is_invalid() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> Todo.builder().withDescription(null).build())
                .withMessage("description cannot be empty");
    }

    @Test
    void invalid_when_contexts_contain_whitespace_characters() {
        TodoBuilder todoBuilder = Todo.builder()
                .withDescription("A task")
                .withContexts(List.of("a invalid context", "valid-context", "invalid\tcontext"));

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(todoBuilder::build)
                .withMessage("invalid contexts found [a invalid context, invalid\tcontext] - only non whitespace characters are valid");

    }

    @Test
    void invalid_when_projects_contain_whitespace_characters() {
        TodoBuilder todoBuilder = Todo.builder()
                .withDescription("A task")
                .withProjects(List.of("a invalid project", "!!valid-Project-122", "invalid\tproject"));

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(todoBuilder::build)
                .withMessage("invalid projects found [a invalid project, invalid\tproject] - only non whitespace characters are valid");
    }

    @Test
    void invalid_when_metatag_or_value_contain_whitspace_character_or_colons() {
        TodoBuilder todoBuilder = Todo.builder()
                .withDescription("A task")
                .withMetadata(new HashMap<>() {{
                    put("illegal key", "legal-value!1");
                    put("illegal:key", "legal-value");
                    put("legal-key", "illegal value");
                }});

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(todoBuilder::build)
                .withMessage("invalid metatags found {illegal key=legal-value!1, legal-key=illegal value, illegal:key=legal-value} - only non whitespace characters which are not colons are valid");
    }

    @Test
    void invalid_when_completed_with_creation_date_but_no_completion_date() {
        TodoBuilder todoBuilder = Todo.builder()
                .isCompleted(true)
                .withDescription("A urgent task")
                .withCreationDate(LocalDate.of(2022, Month.OCTOBER, 12));

        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(todoBuilder::build)
                .withMessage("completed task requires completion date if creation date is present");
    }

    @Test
    void invalid_when_completed_with_completion_date_before_creation_date() {
        TodoBuilder todoBuilder = Todo.builder()
                .isCompleted(true)
                .withDescription("A urgent task")
                .withCompletionDate(LocalDate.of(2022, Month.OCTOBER, 12))
                .withCreationDate(LocalDate.of(2022, Month.OCTOBER, 13));

        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(todoBuilder::build)
                .withMessage("completion date '2022-10-12' before creation date '2022-10-13'");
    }
}
