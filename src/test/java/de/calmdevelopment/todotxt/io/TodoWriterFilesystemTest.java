/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt.io;

import de.calmdevelopment.todotxt.Prio;
import de.calmdevelopment.todotxt.Todo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TodoWriterFilesystemTest {
    @Test
    void should_write_stream_of_todos_to_file(@TempDir Path todoDir) {
        var todoFile = todoDir.resolve("todo.txt");

        var todos = List.of(
                Todo.builder().withDescription("A Test task").build(),
                Todo.builder().withPrio(Prio.A).withDescription("A task with a prio").build(),
                Todo.builder().withDescription("A Test task with +project").withProjects(List.of("project")).build(),
                Todo.builder().withDescription("A Test task with @context").withContexts(List.of("context")).build(),
                Todo.builder().withDescription("A Test task with meta:data").withMetadata(new HashMap<>() {{
                    put("meta", "data");
                }}).build(),
                Todo.builder().withPrio(Prio.B).withCreationDate(LocalDate.parse("2022-10-10")).withDescription("A Test task with prio and creation date").build(),
                Todo.builder().withCreationDate(LocalDate.parse("2022-10-11")).withDescription("A Test task with creation date and no prio").build(),
                Todo.builder().withPrio(Prio.A).withCreationDate(LocalDate.parse("2022-10-12")).withDescription("A Test task with everything +project @context meta:data").withProjects(List.of("project")).withContexts(List.of("context")).withMetadata(new HashMap<>() {{
                    put("meta", "data");
                }}).build()
        );

        try (TodoWriter writer = new TodoWriter(todoFile)) {
            writer.printAll(todos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        assertThat(todoFile).content().isEqualTo("""
                A Test task
                (A) A task with a prio
                A Test task with +project
                A Test task with @context
                A Test task with meta:data
                (B) 2022-10-10 A Test task with prio and creation date
                2022-10-11 A Test task with creation date and no prio
                (A) 2022-10-12 A Test task with everything +project @context meta:data
                """);
    }

    @Test
    void should_be_possible_to_append_todo_to_existing_file(@TempDir Path todoDir) throws IOException {
        var todoFile = todoDir.resolve("todo.txt");

        var existingTodos = """
                A task
                Another task
                x some closed task
                """;

        Files.write(todoFile, List.of(existingTodos.split("\n")), StandardOpenOption.CREATE);

        try (TodoWriter writer = new TodoWriter(todoFile, true)) {
            writer.println(Todo.builder().withDescription("An Additional Task").build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        assertThat(todoFile).content().isEqualTo("""
                A task
                Another task
                x some closed task
                An Additional Task
                """);

    }
}
