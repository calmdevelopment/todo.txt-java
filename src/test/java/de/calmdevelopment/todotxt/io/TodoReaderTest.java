/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;

public class TodoReaderTest {
    @Test
    void should_read_todos_from_inputstream() throws Exception {
        var todoTxtContent = """
                A Test task
                (A) A task with a prio
                A Test task with +project
                A Test task with @context
                A Test task with meta:data
                (B) 2022-10-10 A Test task with prio and creation date
                2022-10-11 A Test task with creation date and no prio
                (A) 2022-10-12 A Test task with everything +project @context meta:data
                """;

        try (TodoReader reader = new TodoReader(new ByteArrayInputStream(todoTxtContent.getBytes()))) {
            var todos = reader.all();

            assertThat(todos).hasSize(8);
            assertThat(reader.hasErrors()).isFalse();
        }
    }

    @Test
    void should_skip_invalid_todos() throws Exception {
        var todoTxtContent = """
                A Test task
                (A) A task with a prio
                                
                A Test task with +project
                   
                A Test task with @context
                A Test task with meta:data
                2022-13-11 Illegal date
                (B) 2022-10-10 A Test task with prio and creation date
                2022-10-11 A Test task with creation date and no prio
                (A) 2022-10-12 A Test task with everything +project @context meta:data
                """;

        try (TodoReader reader = new TodoReader(new ByteArrayInputStream(todoTxtContent.getBytes()))) {
            var todos = reader.all();

            assertThat(todos).hasSize(8);
        }
    }

    @Test
    void should_collect_read_errors() throws Exception {
        var todoTxtContent = """
                A Test task
                (A) A task with a prio
                                
                A Test task with +project
                   
                A Test task with @context
                A Test task with meta:data
                2022-13-11 Illegal date
                (B) 2022-10-10 A Test task with prio and creation date
                2022-10-11 A Test task with creation date and no prio
                (A) 2022-10-12 A Test task with everything +project @context meta:data
                """;
        try (TodoReader reader = new TodoReader(new ByteArrayInputStream(todoTxtContent.getBytes()))) {
            var todos = reader.all();

            assertThat(reader.hasErrors()).isTrue();
            assertThat(reader.errors()).containsExactly(
                    new ReadError(3, "An empty line is invalid", ""),
                    new ReadError(5, "An empty line is invalid", ""),
                    new ReadError(
                            8,
                            "Invalid date (format: yyyy-mm-dd). Text '2022-13-11' could not be parsed: Invalid value for MonthOfYear (valid values 1 - 12): 13",
                            "2022-13-11 Illegal date"
                    )
            );
        }
    }

    @Test
    void close_method_is_idempotent() throws Exception {
        try (TodoReader reader = new TodoReader(new ByteArrayInputStream("".getBytes()))) {
            assertThatNoException().isThrownBy(reader::close);
        }
    }
}
