/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TodoReaderFilesystemTest {
    @Test
    void should_read_todos_from_file(@TempDir Path todoFolder) throws Exception {
        Path todoFile = todoFolder.resolve("todo.txt");
        var todosContent = List.of(
                "A Test task",
                "(A) A task with a prio",
                "A Test task with +project",
                "A Test task with @context",
                "A Test task with meta:data",
                "(B) 2022-10-10 A Test task with prio and creation date",
                "2022-10-11 A Test task with creation date and no prio",
                "(A) 2022-10-12 A Test task with everything +project @context meta:data"
        );
        Files.write(todoFile, todosContent);
        try (TodoReader reader = new TodoReader(todoFile)) {
            var todos = reader.all();

            assertThat(todos).hasSize(8);
        }
    }
}
