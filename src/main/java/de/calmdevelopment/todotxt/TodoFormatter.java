/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

public class TodoFormatter {
    public static String format(Todo todo) {
        StringBuilder builder = new StringBuilder();
        appendCompleted(todo, builder);
        appendPrio(todo, builder);
        appendCompletionDate(todo, builder);
        appendCreationDate(todo, builder);
        appendDescription(todo, builder);
        return builder.toString().trim();
    }

    private static void appendCompleted(Todo todo, StringBuilder builder) {
        if (todo.completed()) {
            builder.append("x ");
        }
    }

    private static void appendPrio(Todo todo, StringBuilder builder) {
        if (todo.prio() != Prio.NONE) {
            builder.append(String.format("(%s) ", todo.prio()));
        }
    }

    private static void appendCreationDate(Todo todo, StringBuilder builder) {
        if (todo.creationDate() != null) {
            builder.append(String.format("%s ", todo.creationDate()));
        }
    }

    private static void appendCompletionDate(Todo todo, StringBuilder builder) {
        if (todo.completionDate() != null) {
            builder.append(String.format("%s ", todo.completionDate()));
        }
    }

    private static void appendDescription(Todo todo, StringBuilder builder) {
        builder.append(String.format("%s ", todo.description()));
    }
}
