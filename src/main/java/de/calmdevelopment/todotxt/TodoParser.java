/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import de.calmdevelopment.todotxt.exception.InvalidTodoFormat;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TodoParser {

    private static final String METADATA_TAG_SEPARATOR = ":";
    private static final String CREATION_GROUP_NAME = "creation";
    private static final String COMPLETION_GROUP_NAME = "completion";
    private static final String DESCRIPTION_GROUP_NAME = "description";
    private static final String PRIO_GROUP_NAME = "prio";
    private static final String COMPLETED_GROUP_NAME = "completed";
    private static final String META_DATA_REGEX = "([\\S]+:[\\S]+)";
    private static final String STRICT_META_DATA_REGEX = "([\\S&&[^:]]+:[\\S&&[^:]]+)";
    private static final String URL_REGEX = "\\S+://\\S+";

    private final Pattern todoPattern = Pattern.compile("^(?<completed>x )?(\\((?<prio>[A-Z])\\) )?((?<completion>\\d{4}+-\\d{2}+-\\d{2}) )?((?<creation>\\d{4}+-\\d{2}+-\\d{2}) )?(?<description>.+)$");
    private final Pattern contextPattern = Pattern.compile("( @(\\S+))");
    private final Pattern projectPattern = Pattern.compile("( \\+(\\S+))");
    private final Pattern metadataPattern = Pattern.compile("( " + META_DATA_REGEX + ")");

    /**
     * Parse a Text and transform it into a {@link Todo}
     *
     * @param todoLine a Text representing a Todo
     * @return a Todo
     * @throws InvalidTodoFormat if todoLine has not a valid Todo format
     */
    public Todo parse(String todoLine) {
        if (todoLine == null) {
            throw new InvalidTodoFormat("Null reference is invalid");
        }
        if (todoLine.isBlank()) {
            throw new InvalidTodoFormat("An empty line is invalid");
        }

        try {
            var matcher = todoPattern.matcher(todoLine);
            if (matcher.matches()) {
                var completed = parseCompleted(matcher);
                var prio = parsePrio(matcher);
                var creation = parseCreationDate(matcher, completed);
                var completion = parseCompletionDate(matcher, completed);
                var description = parseDescription(matcher, completed);
                var contexts = parseContexts(todoLine);
                var projects = parseProjects(todoLine);
                var metadata = parseMetadata(todoLine);

                return Todo.builder()
                        .isCompleted(completed)
                        .withPrio(prio)
                        .withCompletionDate(completion)
                        .withCreationDate(creation)
                        .withDescription(description)
                        .withContexts(contexts)
                        .withProjects(projects)
                        .withMetadata(metadata)
                        .build();
            } else {
                throw new InvalidTodoFormat(String.format("Todo does not match the format pattern. given: '%s'", todoLine));
            }
        } catch (DateTimeParseException ex) {
            throw new InvalidTodoFormat("Invalid date (format: yyyy-mm-dd). " + ex.getMessage(), ex);
        }
    }

    private boolean parseCompleted(Matcher matcher) {
        return matcher.group(COMPLETED_GROUP_NAME) != null;
    }

    private Prio parsePrio(Matcher matcher) {
        String prio = matcher.group(PRIO_GROUP_NAME);
        return Prio.from(prio);
    }

    private LocalDate parseCompletionDate(Matcher matcher, boolean completed) {
        var completionString = matcher.group(COMPLETION_GROUP_NAME);
        if (completed) {
            if (completionString != null) {
                return LocalDate.parse(completionString);
            }
        }
        return null;
    }

    private LocalDate parseCreationDate(Matcher matcher, boolean completed) {
        var creationString = matcher.group(CREATION_GROUP_NAME);
        var completionString = matcher.group(COMPLETION_GROUP_NAME);
        if (!completed && completionString != null) {
            return LocalDate.parse(completionString);
        }
        if (creationString != null) {
            return LocalDate.parse(creationString);
        }
        return null;
    }

    private String parseDescription(Matcher matcher, boolean completed) {
        String descriptionString = matcher.group(DESCRIPTION_GROUP_NAME);
        String completionString = matcher.group(COMPLETION_GROUP_NAME);
        String creationString = matcher.group(CREATION_GROUP_NAME);

        if (!completed && completionString != null && creationString != null) {
            descriptionString = creationString + " " + descriptionString;
        }
        return descriptionString.trim();
    }

    private List<String> parseContexts(String todoLine) {
        return getTokensByPattern(todoLine, contextPattern);
    }

    private List<String> parseProjects(String todoLine) {
        return getTokensByPattern(todoLine, projectPattern);
    }

    private Map<String, String> parseMetadata(String todoLine) {
        var metadataStrings = getTokensByPattern(todoLine, metadataPattern);
        return metadataStrings.stream()
                .filter(TodoParser::noURL)
                .filter(TodoParser::strictMetadatMatches)
                .map(it -> it.split(METADATA_TAG_SEPARATOR))
                .collect(Collectors.toMap(it -> it[0], it -> it[1]));
    }

    private static boolean strictMetadatMatches(String metadataMatch) {
        return Pattern.matches(STRICT_META_DATA_REGEX, metadataMatch);
    }

    private static boolean noURL(String metadataMatch) {
        return !Pattern.matches(URL_REGEX, metadataMatch);
    }

    private static List<String> getTokensByPattern(String todoLine, Pattern pattern) {
        List<String> tokens = new ArrayList<>();
        var matcher = pattern.matcher(todoLine);

        while (matcher.find()) {
            tokens.add(matcher.group(2));
        }
        return tokens;
    }
}
