/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TodoValidator {

    private static final String NON_WHITESPACE_PATTERN = "\\S+";
    private static final String NON_WHITESPACE_WITHOUT_COLON_PATTERN = "[\\S&&[^:]]+";

    public static void validMetadata(Map<String, String> metadata) {
        Map<String, String> illegalMetadata = new HashMap<String, String>();
        Pattern keyValueMatcher = Pattern.compile(NON_WHITESPACE_WITHOUT_COLON_PATTERN);

        for (var entry : metadata.entrySet()) {
            if (!keyValueMatcher.matcher(entry.getKey()).matches() || !keyValueMatcher.matcher(entry.getValue()).matches()) {
                illegalMetadata.put(entry.getKey(), entry.getValue());
            }
        }
        if (!illegalMetadata.isEmpty()) {
            throw new IllegalArgumentException(String.format("invalid metatags found %s - only non whitespace characters which are not colons are valid", illegalMetadata));
        }
    }

    static void validDescription(String description) {
        if (description == null || description.isBlank()) {
            throw new IllegalArgumentException("description cannot be empty");
        }
    }

    static void validContexts(List<String> contexts) {
        var illegalContexts = new ArrayList<String>();
        for (String context : contexts) {
            if (!Pattern.matches(NON_WHITESPACE_PATTERN, context)) {
                illegalContexts.add(context);
            }
        }
        if (!illegalContexts.isEmpty()) {
            throw new IllegalArgumentException(String.format("invalid contexts found %s - only non whitespace characters are valid", illegalContexts));
        }
    }

    static void validProjects(List<String> projects) {
        var illegalProjects = new ArrayList<String>();
        for (String project : projects) {
            if (!Pattern.matches(NON_WHITESPACE_PATTERN, project)) {
                illegalProjects.add(project);
            }
        }

        if (!illegalProjects.isEmpty()) {
            throw new IllegalArgumentException(String.format("invalid projects found %s - only non whitespace characters are valid", illegalProjects));
        }
    }

    static void validDates(boolean completed, LocalDate creationDate, LocalDate completionDate) {
        if (completed) {
            if (creationDate != null && completionDate == null) {
                throw new IllegalStateException("completed task requires completion date if creation date is present");
            }
            if (completionDate != null && creationDate != null && completionDate.isBefore(creationDate)) {
                throw new IllegalStateException(String.format("completion date '%s' before creation date '%s'", completionDate, creationDate));
            }
        }
    }
}