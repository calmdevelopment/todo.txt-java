/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class TodoBuilder {
    private Prio prio = Prio.NONE;
    private String description = "";
    private LocalDate creationDate;
    private LocalDate completionDate;
    private List<String> contexts;
    private List<String> projects;
    private boolean completed;
    private Map<String, String> metadata;

    public TodoBuilder withPrio(Prio prio) {
        this.prio = prio;
        return this;
    }

    public TodoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public TodoBuilder withCompletionDate(LocalDate completion) {
        this.completionDate = completion;
        return this;
    }

    public TodoBuilder withCreationDate(LocalDate date) {
        this.creationDate = date;
        return this;
    }

    public TodoBuilder withContexts(List<String> contexts) {
        this.contexts = contexts;
        return this;
    }

    public TodoBuilder withProjects(List<String> projects) {
        this.projects = projects;
        return this;
    }

    public TodoBuilder isCompleted(boolean completed) {
        this.completed = completed;
        return this;
    }

    public Todo build() {
        return new Todo(
                completed, prio,
                description,
                completionDate, creationDate,
                projects, contexts,
                metadata
        );
    }

    public TodoBuilder withMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
        return this;
    }

    public TodoBuilder from(Todo todo) {
        completed = todo.completed();
        prio = todo.prio();
        completionDate = todo.completionDate();
        creationDate = todo.creationDate();
        description = todo.description();
        projects = List.copyOf(todo.projects());
        contexts = List.copyOf(todo.contexts());
        metadata = Map.copyOf(todo.metadata());
        return this;
    }
}
