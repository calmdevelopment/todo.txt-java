/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt;

import java.time.LocalDate;
import java.util.*;

public record Todo(
        boolean completed, Prio prio,
        String description,
        LocalDate completionDate, LocalDate creationDate,
        List<String> projects, List<String> contexts,
        Map<String, String> metadata
) {

    public Todo {
        if (contexts == null) {
            contexts = new ArrayList<>();
        }
        if (projects == null) {
            projects = new ArrayList<>();
        }
        if (metadata == null) {
            metadata = new HashMap<>();
        }
        if (prio == null) {
            prio = Prio.NONE;
        }

        TodoValidator.validDescription(description);
        TodoValidator.validContexts(contexts);
        TodoValidator.validProjects(projects);
        TodoValidator.validMetadata(metadata);
        TodoValidator.validDates(completed, creationDate, completionDate);
        projects = Collections.unmodifiableList(projects);
        contexts = Collections.unmodifiableList(contexts);
        metadata = Collections.unmodifiableMap(metadata);
    }

    public static TodoBuilder builder() {
        return new TodoBuilder();
    }

    public String full() {
        return TodoFormatter.format(this);
    }

    public Todo prioOf(Prio prio) {
        return builder().from(this).withPrio(prio).build();
    }
}
