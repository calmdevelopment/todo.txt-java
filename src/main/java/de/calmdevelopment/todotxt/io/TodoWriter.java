/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt.io;

import de.calmdevelopment.todotxt.Todo;

import java.io.*;
import java.nio.file.Path;
import java.util.List;

class TodoWriter implements AutoCloseable {

    private Writer writer;

    public TodoWriter(OutputStream out) {
        this.writer = new BufferedWriter(new OutputStreamWriter(out));
    }

    public TodoWriter(Path todoFile) throws IOException {
        this(todoFile, false);
    }

    public TodoWriter(Path todoFile, boolean append) throws IOException {
        this(new FileOutputStream(todoFile.toFile(), append));
    }

    @Override
    public void close() throws IOException {
        writer.close();
        writer = null;
    }

    public void println(Todo todo) throws IOException {
        writer.write(todo.full());
        writer.write(System.lineSeparator());
    }

    public void printAll(List<Todo> todos) throws IOException {
        for (Todo todo : todos) {
            println(todo);
        }
    }
}
