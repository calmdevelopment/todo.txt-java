/*
 * PDX-License-Identifier: Apache-2.0
 *
 * Copyright 2022 Frank Becker.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.calmdevelopment.todotxt.io;

import de.calmdevelopment.todotxt.Todo;
import de.calmdevelopment.todotxt.TodoParser;
import de.calmdevelopment.todotxt.exception.InvalidTodoFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TodoReader implements AutoCloseable {
    private final Logger logger = LoggerFactory.getLogger(TodoReader.class);
    private BufferedReader reader;
    private final List<ReadError> errors = new ArrayList<>();

    public TodoReader(Path todoFile) throws FileNotFoundException {
        this(new FileInputStream(todoFile.toFile()));
    }

    public TodoReader(InputStream inputStream) {
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @Override
    public void close() throws Exception {
        if (reader != null) {
            reader.close();
            reader = null;
            errors.clear();
        }
    }

    public List<Todo> all() throws IOException {
        var parser = new TodoParser();
        var todos = new ArrayList<Todo>();

        var lineNumberReader = new LineNumberReader(reader);

        while (lineNumberReader.ready()) {
            var todoLine = lineNumberReader.readLine();
            try {
                todos.add(parser.parse(todoLine));
            } catch (InvalidTodoFormat itf) {
                int lineNumber = lineNumberReader.getLineNumber();
                String reason = itf.getMessage();
                errors.add(new ReadError(lineNumber, reason, todoLine));
                logger.warn("invalid todo at line {} - reason: {} - original: '{}'", lineNumber, reason, todoLine);
            }
        }
        return todos;
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<ReadError> errors() {
        return Collections.unmodifiableList(errors);
    }
}
