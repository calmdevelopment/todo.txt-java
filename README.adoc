= todo.txt-java
:todotxt-primer: https://github.com/todotxt/todo.txt

A java library implementing a parser for the {todotxt-primer}[todo.txt format]

The library is intended to be used by todo application authors to build tools based on the todo.txt format.

== Format overview

A textual representation of the todo.txt format based on the original image found in the primer.

----

                                 Description; tags (optional)
                                 can be placed anywhere here
                            +------------------------------------------------------+
                            |                                                      |
----

----
x (A) 2016-05-20 2016-04-30 measure space for +chapelShelving @chapel due:2016-05-30
----

----
| | | |        | |        |                   |             | |     | |            |
| +-+ +--------+ +--------+                   +-------------+ +-----+ +------------+
|  |       |          |                               |          |           |
|  |       |          +--> creation date (optional)   |          |           +--> special tag
|  |       |                                          |          |                (key:value)
|  |       +--> completion date (optional)            |          |
|  |                                                  |          +--> context tag
|  +--> priority (optional)                           |
|                                                     +--> project tag
+--> completed (optional)
----

* completed - marks todo as completed (optional) (valid: x)
* priority - marks a priority (optional) (valid: A-Z)
* completion date - the date the task was completed (optional) (valid: yyyy-mm-dd)
* creation date - the date the task was created (optional) (valid: yyyy-mm-dd)
+
IMPORTANT: must be specified if completion date is
* project tag - adds a relationship to a project (optional) (valid: starts with a + followed by non-whitespace characters)
* context tag - adds a context to the todo (optional) (valid: starts with a @ followed by non-whitespace characters)
* special tag - adds metadata to the todo (optional) (valid: key:value; only one colon separates key and value; key and value must consist of non-whitespace characters, which are not colons.
)

The detailed Rules for the format can be read at the original primer of the {todotxt-primer}[todo.txt format]

== Current State

The library is in a very early stage right now.
I think the parser itself is behaving correctly as of the rules described in the primer.
I had my difficulties understanding the date rules to be honest and tried to make the best sense out of it.
Keep a look at the projects link:todo.txt[] file :)

[IMPORTANT]
====
A Todo is a record and fully immutable.
The description will contain the original order of text and tags.
You can get the full string representation of a todo calling the `full()` method.
====

=== My current interpretation of the date handling rule

Both creation date and completion date are optional for incomplete and complete todos.

For incomplete Todos

* The first date that matches the pattern after the priority will be the creation date.
Other dates are interpreted as part of the description

For complete Todos

* The first date will be the completion date and the second (if present) the creation date

If a incomplete task transitions to a completed task and has a creation date present a completion date must be present.
(Keeps completed tasks sortable by standard sort tools)

== Contributions

Contributions are welcome in any form.
I am open for any idea and inspiration of how the library should or shouldn't work.
Help me to shape the future of this library to support working with it in an easy and comfortable way.
Please feel free to open a discussion in form of an issue or leave me a pull/merge request.

== Disclaimer

Your data is yours.
I don't mean harm to your data and beloved todo lists.
For your own peace of mind.
Pleas backup your data.

I am doing this in my free time, mostly after my day job.
So please be patient with me if I don't react directly.
I have a life to live.